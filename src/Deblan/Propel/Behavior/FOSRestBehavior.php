<?php

namespace Deblan\Propel\Behavior;

use Propel\Generator\Model\Behavior;
use Propel\Generator\Builder\Om\ObjectBuilder;
use Propel\Generator\Model\Column;
use Exception;

/**
 * Class FOSRestBehavior
 * @author Simon Vieille <simon@deblan.fr>
 */
class FOSRestBehavior extends Behavior
{
    /**
     * {@inheritdoc}
     */
    public function objectFilter(&$script) 
    {
        $script = preg_replace(
            '#(\s+)\*/(\s+)abstract class#',
            '$1'.$this->getClassAnnotations().'*/$2abstract class',
            $script
        );
    }

    /**
     * {@inheritdoc}
     */
    public function objectMethods()
    {
        $script = '';

        foreach ($this->getTable()->getColumns() as $column) {
            $name = ucfirst($column->getName());
            
            try {
                $groups = $this->transformGroups($this->getParameter($column->getName()));
                $getter = $this->getColumnGetter($column);
                $newGetter = preg_replace('/^get/', 'getRest', $getter);
                $script.= $this->generateRestMethod($column, $newGetter, $getter, $groups);
            } catch (Exception $e) {
            }
        }

        return $script;
    }

    /**
     * Transform the group list to parameter format of annotation
     * 
     * @param string $groups
     * @return string
     */
    protected function transformGroups($groups) 
    {
        $data = [];

        foreach (explode(',', $groups) as $group) {
            $group = trim($group);
        
            if ('' !== $group) {
                $data[] = '"'.$group.'"';
            }
        }

        return sprintf('{%s}', implode(', ', $data));
    }

    /**
     * Generates rest method
     *
     * @param Column $column
     * @param string $newGetter
     * @param string $getter
     * @param string $groups
     * @return string
     */
    public function generateRestMethod(Column $column, $newGetter, $getter, $groups)
    {
        $annotations = [
            '/**',
            ' * @JMS\Serializer\Annotation\SerializedName("'.$column->getName().'")',
        ];

        if ($groups) {
            $annotations[] = ' * @JMS\Serializer\Annotation\Groups('.$groups.')';
        }
            
        $annotations[] = ' * @JMS\Serializer\Annotation\VirtualProperty';
        $annotations[] = '*/';

        $annotations = implode("\n", $annotations);

        $method = <<<EOS
$annotations
public function $newGetter()
{
    return \$this->$getter();
}


EOS;
    
        return $method;
    }
    
    /**
     * Get the getter of one of the columns of the behavior
     *
     * @param  string $column One of the behavior columns, 'create_column' or 'update_column'
     * @return string The related getter, 'getCreatedOn' or 'getUpdatedOn'
     */
    protected function getColumnGetter($column)
    {
        return 'get' . $column->getPhpName();
    }

    /**
     * Generates code for annotations
     * 
     * @return string
     */
    protected function getClassAnnotations() 
    {
        $annotations = [];

        foreach (['@JMS\Serializer\Annotation\ExclusionPolicy("all")'] as $annotation) {
            $annotations[] = ' * '.$annotation;
        }

        return sprintf("%s\n", implode("\n", $annotations));
    }
}

